# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/helm" {
  version     = "2.4.1"
  constraints = "~> 2.2"
  hashes = [
    "h1:Gqwrr+yKWR79esN39X9eRCddxMNapmaGMynLfjrUJJo=",
    "zh:07517b24ea2ce4a1d3be3b88c3efc7fb452cd97aea8fac93ca37a08a8ec06e14",
    "zh:11ef6118ed03a1b40ff66adfe21b8707ece0568dae1347ddfbcff8452c0655d5",
    "zh:1ae07e9cc6b088a6a68421642c05e2fa7d00ed03e9401e78c258cf22a239f526",
    "zh:1c5b4cd44033a0d7bf7546df930c55aa41db27b70b3bca6d145faf9b9a2da772",
    "zh:256413132110ddcb0c3ea17c7b01123ad2d5b70565848a77c5ccc22a3f32b0dd",
    "zh:4ab46fd9aadddef26604382bc9b49100586647e63ef6384e0c0c3f010ff2f66e",
    "zh:5a35d23a9f08c36fceda3cef7ce2c7dc5eca32e5f36494de695e09a5007122f0",
    "zh:8e9823a1e5b985b63fe283b755a821e5011a58112447d42fb969c7258ed57ed3",
    "zh:8f79722eba9bf77d341edf48a1fd51a52d93ec31d9cac9ba8498a3a061ea4a7f",
    "zh:b2ea782848b10a343f586ba8ee0cf4d7ff65aa2d4b144eea5bbd8f9801b54c67",
    "zh:e72d1ccf8a75d8e8456c6bb4d843fd4deb0e962ad8f167fa84cf17f12c12304e",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.5.1"
  constraints = "~> 2.5.0"
  hashes = [
    "h1:wg+6NFJUKrwccLZQPkJTgdRMgh709ZX+sBuJ9IlJoNU=",
    "zh:05f42363a1bdf1858bfdb56dc55509a066a640d05ef62e0fddcf7cc4d97a0dfa",
    "zh:2b974bebbc7823f759159fcf1efe0717930b7756f643ff1551e2d633a6651d10",
    "zh:329817c534ae807ae88030f78e5c1ad1f124e9d5d7ae2830229a6d18ba7dd22e",
    "zh:3c41ea252ded2788d7c47d613e9c1a6543d61e67905f9b1ebc8be23df556e844",
    "zh:3fdff88596baf4f76dc64e016a5f7ba455b8b0b46b45e124e03074746f6a4d1b",
    "zh:480819de824ab56074c4fcb9ebc5d7c7d44dda5fe6e9a3bc0b858b795f74ad41",
    "zh:498fa4b9a5e73afe7560fe4540553a4e1e6a5691acc9384f0ebee32af0afe698",
    "zh:6dfb17f21cc3dfd5096a1cf0d01603527213b127dade411f79c524d2bddc78b9",
    "zh:b5132f132af0b0927d9cad126909d375aad2c11abed896a345529f1a2680eda8",
    "zh:c0fc51da9dff350d29d3887660e8d390b2402ee415c190e17dbcf271d2234c16",
    "zh:dfb1c8dd3409c7b09e2ffeeeadfa41cad076cc06b0d95a303ee096c01e9e68cf",
  ]
}

provider "registry.terraform.io/scaleway/scaleway" {
  version     = "2.2.0"
  constraints = "~> 2.2"
  hashes = [
    "h1:4M58uMZHO9fYFPBug868UjaDo+EutwtEGSsCHi4E3UM=",
    "zh:016486d3e448630e29595412b8d31c8a3b2cec83fa531a86ef3d3858f6ebe45d",
    "zh:0ae1eb5142f866da8849475d976da21a2f7cc905f37c99e7aef810c3369e0f65",
    "zh:0c2c8bd7b8beb49d6320cbb4dfba647a465dc1d0f3583b01ad7f0b0559cc043a",
    "zh:1a2ddae054c2e21db960d25432406f01ac0aca7529838176c11a892f62987a8f",
    "zh:211c92dd31513b06d679f1d9d85ed39911ee32a5c03a2af93e7a5710cb6d0a64",
    "zh:76ae419f8ebdc39236c14f87b6c03c9b1f2c6f60081fec48d7d3531cadafcdbe",
    "zh:8a951e6fb2c329d0095edb607e5c760c47838956d7b0a75111692bd77158a445",
    "zh:9ff0638bd03a39aacf331912ff1547d31c5a5a3e1961bf3ca50a581ae2bf0cba",
    "zh:a3ece235aefa0a3110ecae34304122fe32a566a9d54123593e9434693b03d764",
    "zh:a47f101c77be03df35179474110ab05397693ce7efb74c5a133ae3cb0041bb88",
    "zh:a69562aaf71b7ef7b2c8ef345637a3309762f45d960560453595ea2ec2cb0e2c",
    "zh:aa4b8699e888b08905271a6a2c5da206ac737742b8a947a1d9ed53c875954cd5",
    "zh:abb3e3e235b4862ead53397dd09be40c91910931a993b9415185ea6e8492fdb4",
    "zh:e772b6ccb6d70a2d610c696185d3411548c73ad6e10870f70921021134ef1e67",
  ]
}
